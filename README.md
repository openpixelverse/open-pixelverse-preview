# Project Preview

This is a basic template written in [vuejs](https://vuejs.org) that I can use to show _something_ on newly purchased project websites.

[[_TOC_]]

# Features ⭐

- Add your project logo 🍋
- MailChimp integration 📧
- Link to Whitepaper 📄
- Customize the Sh** out of this simple blueprint 🤘
- (soon) Contact form with [simpleinbox.io](https://simpleinbox.io)

# Usage

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Setup

Run the following to start the setup wizard and follow the instructions.

```
make init
```

## Configuration

Configuration is done through various files.

* `.env` for the main configuration of docker and Vite
* `vite.config.ts` for Vite specific configuration
* `tsconfig.config.json` for TypeScript specific configuration
* `tailwind.config.js` for Tailwind specific configuration
* `docker-compose.yml` for docker compose configuration (for local development only)

## Development

Run the following to start and stop the containers.

```
make start

make stop
```


### Type-Check, Compile and Minify for Production

```sh
npm run build
```
