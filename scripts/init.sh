#!/usr/bin/env bash

# Load configurations.
source ./scripts/setup-colors.sh

# Function to show help message.
function showHelp() {
    echo -e "${WARN}Usage (Makefile): make init${NC}"
}

# Copy .env.example to .env if there is none yet.
if test ! -f .env; then
    cp .env.example .env
    echo -e "${SUCCESS}.env was created ✅${NC}"
else
    echo -e "${WARN}.env already exists, skipping...${NC}"
fi

while true; do
    read -p "Do you want to remove the '.git' directory? " yn
    case $yn in
        [Yy]* ) rm -rf ./.git; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo -e "${SUCCESS}Done! 🚀${NC}"
echo -e "${WARN}Please update your '.env' file now 🧐${NC}"
echo -e "${WARN}Afterwards you can execute 'make start' to start the service.${NC}"

# Exit sucessfully.
exit 0

