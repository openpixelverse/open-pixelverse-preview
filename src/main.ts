import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './assets/main.css'

// FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
/* import specific icons */
import { faHome, faPaperPlane, faNewspaper, faEnvelope, faAt, faHeart, faAngleUp, faCode, faFaceSmileBeam } from '@fortawesome/free-solid-svg-icons'
import { faTwitter, faYoutube, faGitlab, faGithub } from '@fortawesome/free-brands-svg-icons'
/* add icons to the library */
library.add(faHome, faPaperPlane, faNewspaper, faEnvelope, faAt, faHeart, faAngleUp, faCode, faFaceSmileBeam, faTwitter, faYoutube, faGitlab, faGithub)

const app = createApp(App)

app.use(router)

app.component('fa', FontAwesomeIcon)

app.mount('#app')
